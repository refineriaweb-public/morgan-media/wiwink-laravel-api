# WiWink Laravel API

Paquete de Composer que permite dotar a un proyecto Laravel de las herramientas necesarias para interactuar con la API de la plataforma WiWink.
* [PHP Doc](https://refineriaweb-public.gitlab.io/morgan-media/wiwink-laravel-api/packages/RW-WiWinkApi.html) (en inglés)
* [Api de WiWink](https://wiwink.readme.io/reference) (Para fines documentales)

## Requisitos
* PHP 8.0.0 o superior.
* Laravel 8.12 o superior.
* Extensión CURL de PHP.

## Instalación
Para llevar a cabo la instalación de esta librería en cualquier proyecto que cumpla los requisitos, deben seguirse los pasos que se detallan a continuación:

1. Añadir el repositorio correspondiente como última clave de nuestro archivo `composer.json`.

```json
"repositories": {
    "wiwink-laravel-api": {
        "type": "vcs",
        "url": "https://gitlab.com/refineriaweb-public/morgan-media/wiwink-laravel-api"
    }
}
```

2. Añadir el siguiente elemento dentro de la clave `require`.

```json
"refineriaweb/wiwink-laravel-api": "^1.1.0"
```

3. Ejecutar el comando `composer install`. Si no sucede nada, eliminamos el archivo `composer.lock` y reintentamos.

4. Comprobar que el archivo de configuración `wiwink.php` se ha creado en el directorio `config/`. Si no, ejecutar el comando `php artisan vendor:publish --tag=config --force`. Si no, en última instancia, crearlo manualmente e incluir el siguente contenido:
```php
<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
return [
    'api_key' => env('WIWINK_API_KEY', null)
];

```
5. Asegurarnos de que en el `.env` tenemos definida la clave `WIWINK_API_KEY` con la clave de API proporcionada por WiWink como valor.

## Actualización
### Consumidor
Para actualizar el paquete en caso necesario, basta con ejecutar el comando `composer update`.

### Desarrollador
Cada actualización consiste en una **release** de **[git flow](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow)** asociada su respectiva **[tag](https://git-scm.com/book/en/v2/Git-Basics-Tagging)**.

Con cada push a la rama `master`, se refresca el directorio `.phpdoc` de manera automática mediante las reglas definidas en el archivo `gitlab-cy.yml`, para publicarse posteriormente en **gitlab.io** (ver enlace de la introducción relativo a PHP Doc).

## Uso
La librería está planteada de tal manera que únicamente sea necesario instanciar los clientes correspondientes para ejecutar las acciones necesarias.

Los clientes disponibles son:
* [\RW\WiWinkApi\Clients\LeadsClient](https://gitlab.com/refineriaweb-public/morgan-media/wiwink-laravel-api/-/blob/master/src/Clients/LeadsClient.php)
* [\RW\WiWinkApi\Clients\LeadSourcesClient](https://gitlab.com/refineriaweb-public/morgan-media/wiwink-laravel-api/-/blob/master/src/Clients/LeadSourcesClient.php)
* [\RW\WiWinkApi\Clients\LeadStatusesClient](https://gitlab.com/refineriaweb-public/morgan-media/wiwink-laravel-api/-/blob/master/src/Clients/LeadStatusesClient.php)
* [\RW\WiWinkApi\Clients\CustomersClient](https://gitlab.com/refineriaweb-public/morgan-media/wiwink-laravel-api/-/blob/master/src/Clients/CustomersClient.php)
* [\RW\WiWinkApi\Clients\InvoicesClient](https://gitlab.com/refineriaweb-public/morgan-media/wiwink-laravel-api/-/blob/master/src/Clients/InvoicesClient.php)
* [\RW\WiWinkApi\Clients\InvoiceProductsClient](https://gitlab.com/refineriaweb-public/morgan-media/wiwink-laravel-api/-/blob/master/src/Clients/InvoiceProductsClient.php)

Por ejemplo, imaginemos que tenemos un controlador a modo de <abbr title="Create, Read, Update, Delete">CRUD</abbr> para que nuestra aplicacion interaccione con los endpoints de WiWink necesarios para gestionar un Lead procedente de un servicio externo.
El controlador resultante podría ser el siguiente:

```php
<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use RW\WiWinkApi\Clients\LeadsClient;

use RW\WiWinkApi\Models\Lead;

class LeadsController extends Controller
{
    /**
     * Client instance.
     *
     * @var \RW\WiWinkApi\Clients\LeadsClient;
     */
    private LeadsClient $client;

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        $this->client = new LeadsClient();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request) : JsonResponse
    {
        $data = $request->all();

        return $this->client->list($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id target lead id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id) : JsonResponse
    {
        return $this->client->show($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request the request given.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request) : JsonResponse
    {
        $data = $request->all();

        $lead = new Lead($data);

        return $this->client->store($lead);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request the request given.
     * @param  int  $id target lead id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, Request $request) : JsonResponse
    {
        $data = $request->all();

        $lead = new Lead($data);

        return $this->client->update($id, $lead);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id target lead id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id) : JsonResponse
    {
        return $this->client->destroy($id);
    }
}
```
Cómo se puede apreciar, en él se hacen uso de todos los métodos que posee un cliente, provistos de todos sus correspondientes parámetros.
