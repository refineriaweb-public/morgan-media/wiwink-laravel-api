# Hotfix null_response_edge_case (2021/12/03)
## Fixed
* Fixed an edge case where the response body in `ResultList` can be null.
# Release 1.2.5 (2021/05/07)
## Changes
* Make endpoints names more scalable on client instances.
* Added support for jsonData on `@list` requests.
## Fixed
* Fixed some wrong concatenation between enpoints and IDs.
# Release 1.2.4 (2021/04/14)
## Changes
* Improved exceptions handling to match WiWink's API schemas.
## Fixed
* Fixed huge memory leak when trying to cast a result to class that is an `BaseModelWithCustom` instance.
# Release 1.2.3 (2021/04/14)
## Changes
* Added `Accept: application/json` header into request.
* Custom model attributes are now json encoded.
# Release 1.2.2 (2021/04/13)
## Fixes
* Fixed missing return in `LeadSourcesClient@list`.
# Release 1.2.1 (2021/04/13)
## Fixes
* Fixed wrong `Override` custom PHP 8 Attribute package doc.
* Fixed `BaseModelWithCustom@getAttribute` mismatch.
* Fixed some missing $attributes in `Lead` model.
# Release 1.2.0 (2021/04/11)
## Additions
* Added `CHANGELOG.md` file.
* Added `Override` custom PHP 8 Attribute.
* Added `HasCustomAttributes` trait.
* Added `BaseModelWithCustom` model.
## Changes
* Implemented `Override` custom PHP 8 Attribute in needed cases.
* Set some non-extendable classes as `final`.
* Renamed `Model` class to `BaseModel`.
* Now all base abstract classes are in his matching namespace instead of root namespace.
* Changed `HasAttributes@setAttribute` return type from `void` to `mixed`.
* Updated extension to `BaseModelWithCustom` in models that needed.
## Fixes
* Fixed missing information for some params in `README.md` docblocks.
* Fixed incosistent non-null `body` property handling in `Response` class.
* Fixed wrong request data format.
# Release 1.1.6 (2021/04/06)
## Changes
* Updated `.editorconfig` to accomplish with Refinería Web's standards.
* Removed useless 'result' concept.
* Removed some unnecessary code in custom `ResponseException` exception.
* Improved handling for `DELETE` requests on `WiWinkClient` class.
## Fixes
* Fixed wrong method return types on some dockblocks of `README.md`.
* Fixed some mishandling of `castResult` on all clients.
* Fixed missing `int $id` param on `update` method of all clients.
* Fixed wrong `trace_log` call in `ErrorHandler@generateErrorLogs` method.
# Release 1.1.5 (2021/03/28)
## Fixes
* Fixed some bugs in clients.
# Release 1.1.4 (2021/03/28)
## Changes
* Improved data structure for custom `ListResponse` response.
## Fixed
* Fixed wrong return type of `castResult` method.
# Hotfix missing_semicolon (2021/03/28)
## Fixes
* Fixed a missing semicolon in `WiWinkClient` class.
# Release 1.1.3 (2021/03/28)
## Fixes
* Fixed some bugs related to responses' data handling.
# Release 1.1.2 (2021/03/28)
## Additions
* Added `ApiResponser` trait.
    * Very usefull to unify REST Api responses handling.
    * `WiWinkClient` class uses it.
## Changes
* Refactor all clients to use new `ApiResponser` trait.
* Implemented `ListResponse` support into `WiWinkClient` class.
## Fixes
* Fixed some wrong docblocks.
# Release 1.1.1 (2021/03/28)
## Fixes
* Minor fixes over `WiWinkClient` class.
# Hotfix php_docker_image (2021/03/28)
## Fixes
* Fixed wrong PHP version (8.0 to 7.2) used for docker image defined in `gitlab-ci.yml` file.
# Release 1.1.0 (2021/03/28)
## Additions
* Added a composer autoloader.
* Added support for Laravel service providing.
    * It adds a config file for API Key storing purposes.
* Added `ListResponse` as custom response type.
## Changes
* Finished `README.md` documentation.
* Renamed responses' `result` param to `data`.
* Refactor some clases to support PHP 8 property promotion.
* Changed some `self` return types to PHP 8 `static` type.
## Fixes
* Fixed wrong `$page` param on clients' `list` method.
* Fixed (and removed) wrong `try-catch-finally` structure.
# Release 1.0.3 (2021/03/24)
## Fixes
* Fixed various wrong docblocks.
# Release 1.0.2 (2021/03/23)
## Changes
* Updated `README.md`.
# Release 1.0.1 (2021/03/23)
## Additions
* Added `gitlab-ci.yml`.
    * It sets up `phpDoc` site every thime that `master` gets pushed.
## Fixes
* Fixed phpdoc's build folder gitignoring.
# Release 1.0.0 (2021/03/23)
* Initial release
