<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi;

use RW\WiWinkApi\Exceptions\ResponseException;

/**
 * Class for WiWink responses.
 *
 * @package RW\WiWinkApi
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
class Response
{
    /**
     * The body of the response.
     *
     * @var object
     */
    protected ? object $body;

    /**
     * The exception of the response (if any).
     *
     * @var \RW\WiWinkApi\Exceptions\ResponseException
     */
    protected ResponseException $exception;

    /**
     * Create a new WiWink response instance.
     *
     * @return void
     */
    public function __construct(protected Request $request, string $rawResponse)
    {
        $this->decodeResponse($rawResponse);
    }

    /**
     * Gets response's request.
     *
     * @return \RW\WiWinkApi\Request
     */
    public function getRequest() : Request
    {
        return $this->request;
    }

    /**
     * Gets response's request endpoint.
     *
     * @return string
     */
    public function getEndpoint() : string
    {
        return $this->getRequest()->getEndpoint();
    }

    /**
     * Gets response's body.
     *
     * @return object|null
     */
    public function getBody() : ? object
    {
        return $this->body;
    }

    /**
     * Gets response's body data result.
     *
     * @return array|object|null
     */
    public function getData() : array|object|null
    {
        return $this->getBody()?->data;
    }

    /**
     * Get response's exception.
     *
     * @return \RW\WiWinkApi\Exceptions\ResponseException
     */
    public function getException() : ResponseException
    {
        return $this->exception;
    }

    /**
     * Gets errors from response body (if any)
     *
     * @return object|null
     */
    public function getError() : ? string
    {
        return isset($this->getBody()->errors)
            ? json_encode($this->getBody())
            : null;
    }

    /**
     * Determines if the response is an error.
     *
     * @return bool
     */
    public function isError() : bool
    {
        return !is_null($this->getError());
    }

    /**
     * Makes response's exception.
     *
     * @return \RW\WiWinkApi\Exceptions\ResponseException
     */
    public function makeException() : ResponseException
    {
        $this->exception = new ResponseException($this);

        return $this->exception;
    }

    /**
     * Decodes the entire raw resoponse response and attachs it as body.
     *
     * @param string $rawResponse raw response to decode.
     *
     * @return void
     */
    protected function decodeResponse(string $rawResponse) : void
    {
        $this->body = json_decode($rawResponse);
    }
}
