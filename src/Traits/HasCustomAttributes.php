<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Traits;

/**
 * Adds custom attributes functionality to WiWink models that can have custom fields.
 *
 * @package RW\WiWinkApi\Traits
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
trait HasCustomAttributes
{
    /**
     * The model's custom attributes.
     *
     * @var array
     */
    protected array $customAttributes = [];

    /**
     * The model's mapped custom attributes.
     *
     * @var array
     */
    protected static array $customAttributesMap = [];

    /**
     * Get all custom attributes from the model.
     *
     * @return array
     */
    public function getCustomAttributes() : array
    {
        return $this->customAttributes;
    }

    /**
     * Remove the given custom attribute of the model
     *
     * @param  string  $key key to clear.
     *
     * @return void
     */
    public function clearCustomAttribute(string $key) : void
    {
        unset($this->customAttributes[$key]);
    }

    /**
     * Remove the given custom attributes of the model
     *
     * @param  array  $keys keys to clear.
     *
     * @return void
     */
    public function clearCustomAttributes(array $keys = []) : void
    {
        if (!is_array($keys)) {
            $keys = [$keys];
        }

        if (empty($keys)) {
            $this->customAttributes = [];
        }

        foreach ($keys as $key) {
            $this->clearCustomAttribute($key);
        }
    }

    /**
     * Determine if a custom attribute exists.
     *
     * @param  string  $key key to check.
     *
     * @return bool
     */
    protected function hasCustomAttribute(string $key) : bool
    {
        return array_key_exists($key, $this->customAttributes);
    }

    /**
     * Map a custom attribute system name.
     *
     * @param  string  $key key to map.
     * @param  mixed $value value to map.
     *
     * @return void
     */
    public static function mapCustomAttribute(string $key, mixed $value) : void
    {
        static::$customAttributesMap[static::class][$key] = $value;
    }

    /**
     * Map a list of custom attribute system names.
     *
     * @param  array  $attributes attributes to map.
     *
     * @return void
     */
    public static function mapCustomAttributes(array $attributes) : void
    {
        foreach ($attributes as $key => $value) {
            static::mapCustomAttribute($key, $value);
        }
    }

    /**
     * Unmap a custom attribute system name.
     *
     * @param  string  $key key to unmap.
     *
     * @return void
     */
    public static function unmapCustomAttribute(string $key) : void
    {
        unset(static::$customAttributesMap[static::class][$key]);
    }

    /**
     * Unmap a list of custom attribute system names.
     *
     * @param  array  $keys keys to unmap.
     *
     * @return void
     */
    public static function unmapCustomAttributes(array $keys = []) : void
    {
        if (!is_array($keys)) {
            $keys = [$keys];
        }

        if (empty($keys)) {
            static::$customAttributesMap[static::class] = [];
        }

        foreach ($keys as $key) {
            static::unmapCustomAttribute($key);
        }
    }

    /**
     * Determine if a custom attribute is mapped.
     *
     * @param  string  $key key to check.
     *
     * @return bool
     */
    protected static function hasAttributeMapped(string $key) : bool
    {
        return  array_key_exists(static::class, static::$customAttributesMap) &&
                array_key_exists($key, static::$customAttributesMap[static::class]);
    }
}
