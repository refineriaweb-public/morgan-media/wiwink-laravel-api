<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Traits;

/**
 * Adds attributes functionality to WiWink models.
 *
 * @package RW\WiWinkApi\Traits
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
trait HasAttributes
{
    /**
     * The model's attributes.
     *
     * @var array
     */
    protected array $attributes = [];

    /**
     * Get an attribute from the model.
     *
     * @param string $key target attribute key.
     *
     * @return mixed
     */
    public function getAttribute(string $key) : mixed
    {
        if ($this->hasAttribute($key)) {
            return $this->attributes[$key];
        }

        return null;
    }

    /**
     * Get all attributes from the model.
     *
     * @return array
     */
    public function getAttributes() : array
    {
        return $this->attributes;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param string $key key to set.
     * @param mixed $value value to set.
     *
     * @return mixed
     */
    public function setAttribute(string $key, mixed $value) : void
    {
        if ($this->hasAttribute($key)) {
            $this->attributes[$key] = $value;
        }
    }

    /**
     * Set a list of attributes on the model.
     *
     * @param array $attributes attributes to set.
     *
     * @return void
     */
    public function setAttributes(array $attributes) : void
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    /**
     * Determine if an attribute exists.
     *
     * @param string $key key to check.
     *
     * @return bool
     */
    protected function hasAttribute(string $key) : bool
    {
        return array_key_exists($key, $this->attributes);
    }
}
