<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Traits;

use Illuminate\Http\JsonResponse;

/**
 * This concern will be used for any response we sent to clients.
 *
 * @package RW\WiWinkApi\Traits
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
trait ApiResponser
{
    /**
     * Return a success JSON response.
     *
     * @param  string $message success message.
     * @param  mixed $data response data.
     * @param  int $code HTTP status code.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function success(string $message, mixed $data, int $code = 200) : JsonResponse
    {
        return response()->json(
            [
                'message' => $message,
                'data' => $data
            ],
            $code
        );
    }

    /**
     * Return an error JSON response.
     *
     * @param  string $message error message.
     * @param  int $code HTTP status code.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function error(string $message, int $code = 500) : JsonResponse
    {
        return response()->json(
            [
                'message' => $message
            ],
            $code
        );
    }
}
