<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi;

/**
 * If you are not using Composer to manage class autoloading, here's an autoloader for this package.
 *
 * @package RW\WiWinkApi
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
final class Autoloader
{
    /**
     * Registers RW\WiWinkApi\Autoloader as an SPL autoloader.
     *
     * @param  bool $prepend Whether to prepend the autoloader or not.
     *
     * @return void
     */
    public static function register(bool $prepend) : void
    {
        spl_autoload_register(array(__CLASS__, 'autoload'), true, $prepend);
    }

    /**
     * Handles autoloading of classes.
     *
     * @param  string $class A class name.
     *
     * @return void
     *
     * @throws Exception
     */
    public static function autoload(string $class) : void
    {
        if (strpos($class, 'RW\\WiWinkApi\\') !== 0) {
            return;
        }

        $file = __DIR__ . str_replace(array('RW\\WiWinkApi\\', '\\'), '/', $class) . '.php';
        if (is_file($file)) {
            require $file;
        }
    }
}
