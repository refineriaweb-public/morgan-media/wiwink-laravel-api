<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\CustomResponses;

use RW\WiWinkApi\Request;
use RW\WiWinkApi\Response;

/**
 * Custom response for "list" requests.
 *
 * @package RW\WiWinkApi\CustomResponses
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
final class ListResponse extends Response
{
    /**
     * @inheritDoc
     */
    public function __construct(protected Request $request, string $rawResponse)
    {
        parent::__construct($request, $rawResponse);
    }

    /**
     * Gets response's body links result.
     *
     * @return object|null
     */
    public function getLinks() : ? object
    {
        return $this->getBody()?->links;
    }

    /**
     * Gets response's body meta result.
     *
     * @return object|null
     */
    public function getMeta() : ? object
    {
        return $this->getBody()?->meta;
    }
}
