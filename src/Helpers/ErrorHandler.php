<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Helpers;

use Illuminate\Support\Facades\Log;

use Throwable;

/**
 * Helper that handles thrown exceptions.
 *
 * @package RW\WiWinkApi\Helpers
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
final class ErrorHandler
{
    /**
     * Collects exception data and stores relevant entries into array.
     *
     * @param Throwable $e received exception.
     *
     * @return array
     */
    private static function collectErrorData(Throwable $e) : array
    {
        return [
            'error'  => $e->getMessage(),
            'file'  => $e->getFile(),
            'line'  => $e->getLine(),
            'origin' => request()->ip(),
            'agent'  => request()->userAgent()
        ];
    }

    /**
     * Generates the error log.
     *
     * @param Throwable $e received exception.
     *
     * @return array
     */
    public static function generateErrorLogs(Throwable $e) : void
    {
        $errorData = self::collectErrorData($e);

        Log::error($errorData);
    }
}
