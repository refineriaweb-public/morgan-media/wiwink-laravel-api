<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Helpers;

/**
 * Helper that acts like a enum to map all usable HTTP methods.
 *
 * @package RW\WiWinkApi\Helpers
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
final class HttpHelper
{
    /**
     * Method GET keyword.
     *
     * @var string
     */
    public const METHOD_GET = 'GET';

    /**
     * Method POST keyword.
     *
     * @var string
     */
    public const METHOD_POST = 'POST';

    /**
     * Method PUT keyword.
     *
     * @var string
     */
    public const METHOD_PUT = 'PUT';

    /**
     * Method PATCH keyword.
     *
     * @var string
     */
    public const METHOD_PATCH = 'PATCH';

    /**
     * Method OPTIONS keyword.
     *
     * @var string
     */
    public const METHOD_OPTIONS = 'OPTIONS';

    /**
     * Method DELETE keyword.
     *
     * @var string
     */
    public const METHOD_DELETE = 'DELETE';
}
