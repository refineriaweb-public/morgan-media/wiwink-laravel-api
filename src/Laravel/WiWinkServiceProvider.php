<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Laravel;

use Illuminate\Support\ServiceProvider;

/**
 * Class SharpspringServiceProvider
 *
 * @package RW\WiWinkApi\Laravel
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
final class WiWinkServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->publishes(
            [
                __DIR__ . '/config/wiwink.php' => config_path('wiwink.php'),
            ],
            'config'
        );
    }
}
