<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Interfaces;

/**
 * Interface Jsonable
 *
 * @package RW\WiWinkApi\Interfaces
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
interface Jsonable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @param int $options JSON options.
     *
     * @return string
     */
    public function toJson(int $options = 0) : string;
}
