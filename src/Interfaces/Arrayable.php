<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Interfaces;

/**
 * Interface Arrayable
 *
 * @package RW\WiWinkApi\Interfaces
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
interface Arrayable
{
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray() : array;
}
