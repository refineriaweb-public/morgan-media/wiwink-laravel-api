<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Interfaces;

use Illuminate\Http\JsonResponse;

/**
 * Interface Resourceable
 *
 * @package RW\WiWinkApi\Interfaces
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
interface Resourceable
{
    /**
     * Lists all items with filter support (depends on model).
     *
     * @param array $jsonData body params.
     * @param array $queryParamas params to be queried.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(array $jsonData = [], array $queryParams = []) : JsonResponse;

    /**
     * Shows the specified item (by id).
     *
     * @param int $id item id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id) : JsonResponse;

    /**
     * Stores an item.
     *
     * @param object $fields provided fields.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(object $model) : JsonResponse;

    /**
     * Updates the specified item (by id) with the specified fields.
     *
     * @param int $id item id.
     * @param object $fields provided fields.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, object $model) : JsonResponse;

    /**
     * Deletes an item (by id).
     *
     * @param int $id item id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id) : JsonResponse;
}
