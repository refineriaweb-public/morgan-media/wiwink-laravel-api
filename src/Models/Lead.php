<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Models;

use RW\WiWinkApi\Attributes\Override;

/**
 * The Lead table consists of prospects who are possibly interested in your product.
 *
 * @package RW\WiWinkApi\Models
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
final class Lead extends BaseModelWithCustom
{
    /**
     * @inheritDoc
     */
    #[Override(parent::class, 'attributes', 'property')]
    protected array $attributes = [
        'id',
        'name',
        'corporate_name',
        'trade_name',
        'email',
        'phone',
        'address',
        'city',
        'zipcode',
        'body',
        'date',
        'status_id',
        'source_id'
    ];
}
