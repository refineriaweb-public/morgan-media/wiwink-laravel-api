<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Models;

use RW\WiWinkApi\Attributes\Override;

use RW\WiWinkApi\Traits\HasCustomAttributes;

/**
 * Base class for WiWink models.
 *
 * @package RW\WiWinkApi\Models
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
abstract class BaseModelWithCustom extends BaseModel
{
    use HasCustomAttributes;

    /**
     * Get all primary and custom attributes from the model.
     *
     * @return array
     */
    public function getAllAttributes() : array
    {
        $allAttributes = $this->getAttributes();
        $allAttributes["extra_fields"] = json_encode($this->getCustomAttributes(), JSON_UNESCAPED_UNICODE);

        if (empty($allAttributes["extra_fields"])) {
            unset($allAttributes["extra_fields"]);
        }

        return $allAttributes;
    }

    /**
     * Gets all filled attributes: primary and custom.
     *
     * @return array
     */
    public function getAllFilledAttributes() : array
    {
        $filledAttributes = $this->getFilledAttributes();
        $filledAttributes["extra_fields"] = json_encode($this->getCustomAttributes(), JSON_UNESCAPED_UNICODE);

        if (empty($filledAttributes["extra_fields"])) {
            unset($filledAttributes["extra_fields"]);
        }

        return $filledAttributes;
    }

    /**
     * @inheritDoc
     */
    #[Override(parent::class, 'getAttribute')]
    public function getAttribute(string $key) : mixed
    {
        if (static::hasAttributeMapped($key)) {
            $key = static::$customAttributesMap[static::class][$key];
        }

        if ($this->hasCustomAttribute($key)) {
            return $this->customAttributes[$key];
        }

        return parent::getAttribute($key);
    }

    /**
     * @inheritDoc
     */
    #[Override(parent::class, 'setAttribute')]
    public function setAttribute(string $key, mixed $value) : void
    {
        if ($this->hasAttribute($key)) {
            $this->attributes[$key] = $value;

            return;
        }

        if (static::hasAttributeMapped($key)) {
            $key = static::$customAttributesMap[static::class][$key];
        }

        $this->customAttributes[$key] = $value;
    }

    /**
     * @inheritDoc
     */
    #[Override(parent::class, 'replicate')]
    public function replicate() : static
    {
        $model = parent::replicate();
        $model->customAttributes = $this->customAttributes;

        return $model;
    }

    /**
     * @inheritDoc
     */
    #[Override(parent::class, 'toArray')]
    public function toArray() : array
    {
        return $this->getAllAttributes();
    }
}
