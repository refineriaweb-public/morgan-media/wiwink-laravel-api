<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Models;

use RW\WiWinkApi\Attributes\Override;

/**
 * The Invoices table consists of invoices of your products.
 *
 * @package RW\WiWinkApi\Models
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
final class Invoice extends BaseModelWithCustom
{
    /**
     * @inheritDoc
     */
    #[Override(parent::class, 'attributes', 'property')]
    protected array $attributes = [
        'id',
        'customer_id',
        'prefix',
        'number',
        'suffix',
        'tax_percent',
        'discount_ammount',
        'emission_at',
        'expiration_at'
    ];
}
