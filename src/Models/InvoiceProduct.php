<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Models;

use RW\WiWinkApi\Attributes\Override;

/**
 * The InvoicesProduct table consists of products that your invoices contains.
 *
 * @package RW\WiWinkApi\Models
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
final class InvoiceProduct extends BaseModel
{
    /**
     * @inheritDoc
     */
    #[Override(parent::class, 'attributes', 'property')]
    protected array $attributes = [
        'id',
        'billing_id',
        'name',
        'description',
        'quantity',
        'price'
    ];
}
