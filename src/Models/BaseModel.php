<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Models;

use RW\WiWinkApi\Interfaces\Jsonable;
use RW\WiWinkApi\Interfaces\Arrayable;

use RW\WiWinkApi\Traits\HasAttributes;

use ArrayAccess;
use JsonSerializable;

/**
 * Base class for WiWink models.
 *
 * @package RW\WiWinkApi\Models
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
abstract class BaseModel implements Arrayable, Jsonable, ArrayAccess, JsonSerializable
{
    use HasAttributes;

    /**
     * The model's primary key.
     *
     * @var string
     */
    protected string $primaryKey = 'id';

    /**
     * The model's attributes that can be set null.
     *
     * @var array
     */
    protected array $nullable = [];

    /**
     * Create a new WiWink model instance.
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->initializeAttributes();
        $this->setAttributes($attributes);
    }

    /**
     * Fills the attributes with null of a new model
     *
     * @return void
     */
    private function initializeAttributes() : void
    {
        $this->attributes = array_fill_keys(
            array_keys(
                array_flip($this->attributes)
            ),
            null
        );
    }

    /**
     * Get all nullable attributes from the model.
     *
     * @return array
     */
    public function getNullableAttributes() : array
    {
        return array_intersect_key(
            $this->attributes,
            array_flip($this->nullable)
        );
    }

    /**
     * Get all non nullable attributes from the model.
     *
     * @return array
     */
    public function getNonNullableAttributes() : array
    {
        return array_diff_key(
            $this->attributes,
            array_flip($this->nullable)
        );
    }

    /**
     * Gets all filled attributes.
     *
     * @return array
     */
    public function getFilledAttributes() : array
    {
        return array_merge(
            $this->getNullableAttributes(),
            array_filter(
                $this->getNonNullableAttributes(),
                fn ($attribute) : bool => $attribute !== null
            )
        );
    }

    /**
     * Get the primary key for the model.
     *
     * @return string
     */
    public function getKeyName() : string
    {
        return $this->primaryKey;
    }

    /**
     * Get the value of the model's primary key.
     *
     * @return mixed
     */
    public function getKey() : mixed
    {
        return $this->getAttribute($this->getKeyName());
    }

    /**
     * Set the value of the model's primary key.
     *
     * @param mixed $value key value.
     *
     * @return void
     */
    public function setKey(mixed $value) : void
    {
        $this->setAttribute($this->getKeyName(), $value);
    }

    /**
     * Clone the model into a new, non-existing instance.
     *
     * @return static
     */
    public function replicate() : static
    {
        $model = new static;
        $model->attributes = $this->attributes;
        $model->setKey(null);

        return $model;
    }

    /**
     * Create a new model instance with the given attributes.
     *
     * @param array $attributes model attributes.
     *
     * @return static
     */
    public static function make(array $attributes = []) : static
    {
        return new static($attributes);
    }

    /**
     * Create a new model instance with the given json string.
     *
     * @param string $json JSON string to decode.
     *
     * @return static
     */
    public static function makeFromJson(string $json) : static
    {
        return static::make(json_decode($json, true) ?: []);
    }

    /**
     * Create a new model instance with the given object.
     *
     * @param object $object source object.
     *
     * @return static
     */
    public static function makeFromObject(object $object) : static
    {
        return static::make((array) $object);
    }

    /**
     * Convert the model instance to JSON.
     *
     * @param int $options JSON options.
     *
     * @return string
     */
    public function toJson(int $options = 0) : string
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray() : array
    {
        return $this->getAttributes();
    }

    /**
     * Determine if the given attribute exists.
     *
     * @param mixed $offset offset value.
     *
     * @return bool
     */
    public function offsetExists(mixed $offset) : bool
    {
        return !is_null($this->getAttribute($offset));
    }

    /**
     * Get the value for a given offset.
     *
     * @param mixed $offset offset value.
     *
     * @return mixed
     */
    public function offsetGet(mixed $offset) : mixed
    {
        return $this->getAttribute($offset);
    }

    /**
     * Set the value for a given offset.
     *
     * @param mixed $offset key.
     * @param mixed $value value.
     *
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value) : void
    {
        $this->setAttribute($offset, $value);
    }

    /**
     * Unset the value for a given offset.
     *
     * @param mixed $offset key.
     *
     * @return void
     */
    public function offsetUnset(mixed $offset) : void
    {
        $this->offsetSet($offset, null);
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param string $key target key.
     *
     * @return mixed
     */
    public function __get(string $key) : mixed
    {
        return $this->offsetGet($key);
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param string $key provided key.
     * @param mixed $value provided value.
     *
     * @return void
     */
    public function __set(string $key, mixed $value) : void
    {
        $this->offsetSet($key, $value);
    }

    /**
     * Determine if an attribute exists on the model.
     *
     * @param string $key to check.
     *
     * @return bool
     */
    public function __isset(string $key) : bool
    {
        return $this->offsetExists($key);
    }

    /**
     * Unset an attribute on the model.
     *
     * @param string  $key to unset.
     *
     * @return void
     */
    public function __unset(string $key) : void
    {
        $this->offsetUnset($key);
    }

    /**
     * Convert the model to its string representation.
     *
     * @return string
     */
    public function __toString() : string
    {
        return $this->toJson();
    }
}
