<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Models;

use RW\WiWinkApi\Attributes\Override;

/**
 * The Customer table consists of customers that buy your product.
 *
 * @package RW\WiWinkApi\Models
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
final class Customer extends BaseModelWithCustom
{
    /**
     * @inheritDoc
     */
    #[Override(parent::class, 'attributes', 'property')]
    protected array $attributes = [
        'id',
        'corporate_name',
        'trade_name',
        'phone',
        'email',
        'zipcode',
        'city',
        'address',
        'vat',
        'iban',
        'bic',
        'date'
    ];
}
