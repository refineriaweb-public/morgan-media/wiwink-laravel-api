<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Clients;

use Illuminate\Http\JsonResponse;

use RW\WiWinkApi\Exceptions\ResponseException;

use RW\WiWinkApi\Helpers\HttpHelper;
use RW\WiWinkApi\Helpers\ErrorHandler;

use RW\WiWinkApi\Interfaces\Resourceable;

use RW\WiWinkApi\Models\Invoice;
use RW\WiWinkApi\Models\BaseModelWithCustom;

use Exception;

/**
 * Client that allows Invoice model manipulation.
 *
 * @package RW\WiWinkApi\Clients
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
final class InvoicesClient extends WiWinkClient implements Resourceable
{
    /**
     * @inheritDoc
     */
    private const ENDPOINT = "invoices";

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct(config('wiwink.api_key'));
    }

    /**
     * @inheritDoc
     */
    public function list(array $jsonData = [], array $queryParams = []) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT, HttpHelper::METHOD_GET, $jsonData, $queryParams, true);

            $responseParams = [
                "message" => "Invoices listed successfully.",
                "data" => [
                    "items" => $this->castResult($result["items"], Invoices::class),
                    "links" => $result["links"],
                    "meta" => $result["meta"]
                ]
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to list the invoices.");
        }
    }

    /**
     * @inheritDoc
     */
    public function show(int $id) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_GET);

            $responseParams = [
                "message" => "Invoice show successfully.",
                "data" => $this->castResult(array($result), Invoice::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to show the invoice.");
        }
    }

    /**
     * @inheritDoc
     */
    public function store(object $model) : JsonResponse
    {
        try {
            if (!$model instanceof BaseModelWithCustom) {
                new Exception('The $model param should be an instance of ' . BaseModelWithCustom::class);
            }

            $invoice = $model->getAllFilledAttributes();

            $result = $this->executeCall(self::ENDPOINT, HttpHelper::METHOD_POST, $invoice);

            $responseParams = [
                "message" => "Invoice stored successfully.",
                "data" => $this->castResult(array($result), Invoice::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to store the invoice.");
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, object $model) : JsonResponse
    {
        try {
            if (!$model instanceof BaseModelWithCustom) {
                new Exception('The $model param should be an instance of ' . BaseModelWithCustom::class);
            }

            $invoice = $model->getAllFilledAttributes();

            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_PATCH, $invoice);

            $responseParams = [
                "message" => "Invoice updated successfully.",
                "data" => $this->castResult(array($result), Invoice::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to update the invoice.");
        }
    }

    /**
     * @inheritDoc
     */
    public function destroy(int $id) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_DELETE);

            $responseParams = [
                "message" => "Invoice deleted successfully.",
                "data" => $result
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to delete the invoice.");
        }
    }
}
