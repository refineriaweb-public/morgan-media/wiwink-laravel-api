<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Clients;

use RW\WiWinkApi\CustomResponses\ListResponse;

use RW\WiWinkApi\Exceptions\WiWinkException;

use RW\WiWinkApi\Helpers\HttpHelper;

use RW\WiWinkApi\Traits\ApiResponser;

use RW\WiWinkApi\Request;
use RW\WiWinkApi\Response;

/**
 * Base class for WiWink clients.
 *
 * @package RW\WiWinkApi\Clients
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
abstract class WiWinkClient
{
    use ApiResponser;

    /**
     * Base url where the requests will be made.
     *
     * @var string
     */
    private const URL_POINT = 'https://developers.wiwink.com/api/';

    /**
     * WiWink api key.
     *
     * @var string
     */
    protected string $apiKey;

    /**
     * Stores the response of the last request made.
     *
     * @var \RW\WiWinkApi\Response|null
     */
    protected ? Response $lastResponse;

    /**
     * Create a new WiWink client instance.
     *
     * @param  string|null  $apiKey provided API key.
     *
     * @return void
     */
    public function __construct(? string $apiKey)
    {
        if (is_null($apiKey)) {
            throw new WiWinkException('You must provide a valid API key.', 500);
        }

        $this->setApiKey($apiKey);
    }

    /**
     * Set the client's api key.
     *
     * @param  string $apiKey provided API key.
     *
     * @return static
     */
    public function setApiKey(string $apiKey) : static
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get the client's last response.
     *
     * @return \RW\WiWinkApi\Response|null
     */
    public function getLastResponse() : ? Response
    {
        return $this->lastResponse;
    }

    /**
     * Execute the cURL call
     *
     * @param string $endpoint target endpoint.
     * @param string $method body params.
     * @param array $jsonData body params.
     * @param array $queryParamas params to be queried.
     * @param bool $isListRequest checks if request incomes from a list endpoint.
     *
     * @return array
     *
     * @throws \RW\WiWinkApi\Exceptions\WiWinkException
     * @throws \RW\WiWinkApi\Exceptions\ResponseException
     */
    protected function executeCall(
        string $endpoint,
        string $method,
        array $jsonData = [],
        array $queryParams = [],
        bool $isListRequest = false
    ) : array {
        $request = new Request($this->getUrl($endpoint), $method, $jsonData);

        $data = $request->getEncodedData();

        $ch = curl_init($this->getUrl($endpoint, $queryParams));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            [
                'Accept: application/json',
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data),
                'Authorization: Bearer ' . $this->apiKey
            ]
        );

        $rawResponse = curl_exec($ch);
        curl_close($ch);

        if ($request->getMethod() === HttpHelper::METHOD_DELETE && $rawResponse === "") {
            return [];
        }

        $response = new Response($request, $rawResponse);
        if ($isListRequest) {
            $response = new ListResponse($request, $rawResponse);
        }

        if ($response->isError()) {
            throw $response->makeException();
        }

        $this->lastResponse = $response;

        if ($response instanceof ListResponse) {
            return [
                "items" => $response->getData(),
                "links" => $response->getLinks(),
                "meta" => $response->getMeta()
            ];
        }

        return (array) $response->getData();
    }

    /**
     * Get the URL endpoint
     *
     * @param string $endpoint target endpoint.
     * @param array  $params provided query params.
     *
     * @return string
     */
    private function getUrl(string $endpoint, array $params = []) : string
    {
        $baseUrl = self::URL_POINT . $endpoint;

        $queryParams = http_build_query($params);
        if (strlen($queryParams) > 0) {
            return $baseUrl . '?' . $queryParams;
        }

        return $baseUrl;
    }

    /**
     * Cast the result to the specified class
     *
     * @param  array $result result to cast.
     * @param  string $class target class.
     *
     * @return iterable
     */
    protected function castResult(array $result, string $class) : iterable
    {
        if (!is_array($result)) {
            return $this->cast($result, $class);
        }

        return $this->castCollection($result, $class);
    }

    /**
     * Collection casting
     *
     * @param  array $collection collection to cast.
     * @param  string $class target class.
     *
     * @return iterable
     */
    private function castCollection(array $collection, string $class) : iterable
    {
        return array_map(
            function (array|object $object) use ($class) : object {
                return $this->cast($object, $class);
            },
            $collection
        );
    }

    /**
     * Class casting
     *
     * @param array|object $object object to cast.
     * @param string $class target class.
     *
     * @return object
     */
    private function cast(array|object $object, string $class) : object
    {
        return new $class((array) $object);
    }
}
