<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Clients;

use Illuminate\Http\JsonResponse;

use RW\WiWinkApi\Exceptions\ResponseException;

use RW\WiWinkApi\Helpers\HttpHelper;
use RW\WiWinkApi\Helpers\ErrorHandler;

use RW\WiWinkApi\Interfaces\Resourceable;

use RW\WiWinkApi\Models\Customer;
use RW\WiWinkApi\Models\BaseModelWithCustom;

use Exception;

/**
 * Client that allows Customer model manipulation.
 *
 * @package RW\WiWinkApi\Clients
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
final class CustomersClient extends WiWinkClient implements Resourceable
{
    /**
     * @inheritDoc
     */
    private const ENDPOINT = "customers";

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct(config('wiwink.api_key'));
    }

    /**
     * @inheritDoc
     */
    public function list(array $jsonData = [], array $queryParams = []) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT, HttpHelper::METHOD_GET, $jsonData, $queryParams, true);

            $responseParams = [
                "message" => "Customers listed successfully.",
                "data" => [
                    "items" => $this->castResult($result["items"], Customer::class),
                    "links" => $result["links"],
                    "meta" => $result["meta"]
                ]
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to list the customers.");
        }
    }

    /**
     * @inheritDoc
     */
    public function show(int $id) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_GET);

            $responseParams = [
                "message" => "Customer show successfully.",
                "data" => $this->castResult(array($result), Customer::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to show the customer.");
        }
    }

    /**
     * @inheritDoc
     */
    public function store(object $model) : JsonResponse
    {
        try {
            if (!$model instanceof BaseModelWithCustom) {
                new Exception('The $model param should be an instance of ' . BaseModelWithCustom::class);
            }

            $customer = $model->getAllFilledAttributes();

            $result = $this->executeCall(self::ENDPOINT, HttpHelper::METHOD_PATCH, $customer);

            $responseParams = [
                "message" => "Customer stored successfully.",
                "data" => $this->castResult(array($result), Customer::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to store the customer.");
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, object $model) : JsonResponse
    {
        try {
            if (!$model instanceof BaseModelWithCustom) {
                new Exception('The $model param should be an instance of ' . BaseModelWithCustom::class);
            }

            $customer = $model->getAllFilledAttributes();

            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_PATCH, $customer);

            $responseParams = [
                "message" => "Customer updated successfully.",
                "data" => $this->castResult(array($result), Customer::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to update the customer.");
        }
    }

    /**
     * @inheritDoc
     */
    public function destroy(int $id) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_DELETE);

            $responseParams = [
                "message" => "Customer deleted successfully.",
                "data" => $result
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to delete the customer.");
        }
    }
}
