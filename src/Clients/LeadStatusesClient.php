<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Clients;

use Illuminate\Http\JsonResponse;

use RW\WiWinkApi\Exceptions\ResponseException;

use RW\WiWinkApi\Helpers\HttpHelper;
use RW\WiWinkApi\Helpers\ErrorHandler;

use RW\WiWinkApi\Interfaces\Resourceable;

use RW\WiWinkApi\Models\BaseModel;
use RW\WiWinkApi\Models\LeadStatus;

use Exception;

/**
 * Client that allows LeadStatus model manipulation.
 *
 * @package RW\WiWinkApi\Clients
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
final class LeadStatusesClient extends WiWinkClient implements Resourceable
{
    /**
     * @inheritDoc
     */
    private const ENDPOINT = "leads/statuses";

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct(config('wiwink.api_key'));
    }

    /**
     * @inheritDoc
     */
    public function list(array $jsonData = [], array $queryParams = []) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT, HttpHelper::METHOD_GET, $jsonData, $queryParams, true);

            $responseParams = [
                "message" => "Lead statuses listed successfully.",
                "data" => [
                    "items" => $this->castResult($result["items"], LeadStatus::class),
                    "links" => $result["links"],
                    "meta" => $result["meta"]
                ]
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to list the lead statuses.");
        }
    }

    /**
     * @inheritDoc
     */
    public function show(int $id) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_GET);

            $responseParams = [
                "message" => "Lead status show successfully.",
                "data" => $this->castResult(array($result), LeadStatus::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to show the lead status.");
        }
    }

    /**
     * @inheritDoc
     */
    public function store(object $model) : JsonResponse
    {
        try {
            if (!$model instanceof BaseModel) {
                new Exception('The $model param should be an instance of ' . BaseModel::class);
            }

            $leadStatus = $model->getFilledAttributes();

            $result = $this->executeCall(self::ENDPOINT, HttpHelper::METHOD_POST, $leadStatus);

            $responseParams = [
                "message" => "Lead status stored successfully.",
                "data" => $this->castResult(array($result), LeadStatus::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to store the lead status.");
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, object $model) : JsonResponse
    {
        try {
            if (!$model instanceof BaseModel) {
                new Exception('The $model param should be an instance of ' . BaseModel::class);
            }

            $leadStatus = $model->getFilledAttributes();

            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_PATCH, $leadStatus);

            $responseParams = [
                "message" => "Lead status updated successfully.",
                "data" => $this->castResult(array($result), LeadStatus::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            $responseParams = [
                "message" => "An error has been occurred while trying to update the lead status."
            ];

            return $this->error("An error has been occurred while trying to update the lead status.");
        }
    }

    /**
     * @inheritDoc
     */
    public function destroy(int $id) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_DELETE);

            $responseParams = [
                "message" => "Lead status deleted successfully.",
                "data" => $result
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("Lead status deleted successfully.");
        }
    }
}
