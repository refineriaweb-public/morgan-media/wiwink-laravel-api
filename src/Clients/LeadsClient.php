<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Clients;

use Illuminate\Http\JsonResponse;

use RW\WiWinkApi\Exceptions\ResponseException;

use RW\WiWinkApi\Helpers\HttpHelper;
use RW\WiWinkApi\Helpers\ErrorHandler;

use RW\WiWinkApi\Interfaces\Resourceable;

use RW\WiWinkApi\Models\Lead;
use RW\WiWinkApi\Models\BaseModelWithCustom;

use Exception;

/**
 * Client that allows Leads model manipulation.
 *
 * @package RW\WiWinkApi\Clients
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
final class LeadsClient extends WiWinkClient implements Resourceable
{
    /**
     * @inheritDoc
     */
    private const ENDPOINT = "leads";

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct(config('wiwink.api_key'));
    }

    /**
     * @inheritDoc
     */
    public function list(array $jsonData = [], array $queryParams = []) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT, HttpHelper::METHOD_GET, $jsonData, $queryParams, true);

            $responseParams = [
                "message" => "Leads listed successfully.",
                "data" => [
                    "items" => $this->castResult($result["items"], Lead::class),
                    "links" => $result["links"],
                    "meta" => $result["meta"]
                ]
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to list the leads.");
        }
    }

    /**
     * @inheritDoc
     */
    public function show(int $id) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_GET);

            $responseParams = [
                "message" => "Lead show successfully.",
                "data" => $this->castResult(array($result), Lead::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to show the lead.");
        }
    }

    /**
     * @inheritDoc
     */
    public function store(object $model) : JsonResponse
    {
        try {
            if (!$model instanceof BaseModelWithCustom) {
                new Exception('The $model param should be an instance of ' . BaseModelWithCustom::class);
            }

            $lead = $model->getAllFilledAttributes();

            $result = $this->executeCall(self::ENDPOINT, HttpHelper::METHOD_POST, $lead);

            $responseParams = [
                "message" => "Lead stored successfully.",
                "data" => $this->castResult(array($result), Lead::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to store the lead.");
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, object $model) : JsonResponse
    {
        try {
            if (!$model instanceof BaseModelWithCustom) {
                new Exception('The $model param should be an instance of ' . BaseModelWithCustom::class);
            }

            $lead = $model->getAllFilledAttributes();

            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_PATCH, $lead);

            $responseParams = [
                "message" => "Lead updated successfully.",
                "data" => $this->castResult(array($result), Lead::class)
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to update the lead.");
        }
    }

    /**
     * @inheritDoc
     */
    public function destroy(int $id) : JsonResponse
    {
        try {
            $result = $this->executeCall(self::ENDPOINT . "/" . $id, HttpHelper::METHOD_DELETE);

            $responseParams = [
                "message" => "Lead deleted successfully.",
                "data" => $result
            ];

            return $this->success(...$responseParams);
        } catch (ResponseException $e) {
            ErrorHandler::generateErrorLogs($e);

            return $this->error("An error has been occurred while trying to delete the lead.");
        }
    }
}
