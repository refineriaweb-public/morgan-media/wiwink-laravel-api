<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Exceptions;

use Exception;

/**
 * Base class for WiWink exceptions.
 *
 * @package RW\WiWinkApi\Exceptions
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
class WiWinkException extends Exception
{
}
