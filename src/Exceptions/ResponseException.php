<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Exceptions;

use RW\WiWinkApi\Response;

/**
 * Class that handles exceptions produced in responses.
 *
 * @package RW\WiWinkApi\Exceptions
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
final class ResponseException extends WiWinkException
{
    /**
     * Exception data from the response.
     *
     * @var object
     */
    protected object $data;

    /**
     * Create a new WiWink response exception instance.
     *
     * @param \RW\WiWinkApi\Response $response response to catch.
     * @param \RW\WiWinkApi\Exceptions\WiWinkException|null previous state.
     *
     * @return void
     */
    public function __construct(protected Response $response, ? WiWinkException $previous = null)
    {
        $this->response = $response;

        parent::__construct($response->getError(), 500, $previous);
    }

    /**
     * Gets the exception response.
     *
     * @return \RW\WiWinkApi\Response
     */
    public function getResponse() : Response
    {
        return $this->response;
    }
}
