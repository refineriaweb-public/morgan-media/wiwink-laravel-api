<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi;

/**
 * Class for WiWink requests.
 *
 * @package RW\WiWinkApi
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 */
class Request
{
    /**
     * Create a new WiWink request instance.
     *
     * @param string $endpoint endpoint to fetch.
     * @param string $method requested method.
     * @param array $params params to send.
     *
     * @return void
     */
    public function __construct(protected string $endpoint, protected string $method, protected array $params)
    {
    }

    /**
     * Gets the request's endpoint.
     *
     * @return string
     */
    public function getEndpoint() : string
    {
        return $this->endpoint;
    }

    /**
     * Gets the request's method.
     *
     * @return string
     */
    public function getMethod() : string
    {
        return $this->method;
    }

    /**
     * Gets the request's params.
     *
     * @return array
     */
    public function getParams() : array
    {
        return $this->params;
    }

    /**
     * Gets the request properties and encodes it.
     *
     * @return string|bool
     */
    public function getEncodedData() : string|bool
    {
        return json_encode($this->getParams());
    }
}
