<?php
/**
 * This file is part of the refineriaweb/wiwink-laravel-api package.
 *
 * (c) Josep Salvà <jsalva@refineriaweb.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RW\WiWinkApi\Attributes;

use Attribute;
use Exception;

/**
 * Custom PHP Attribute that checks method overriding.
 *
 * @package RW\WiWinkApi\Attributes
 *
 * @author Josep Salvà <jsalva@refineriaweb.com>
 *
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_PROPERTY)]
final class Override
{
    public function __construct(string $object, string $target, string $type = 'method')
    {
        switch ($type) {
            case 'method':
                if (!method_exists($object, $target)) {
                    throw new Exception('The method that you\'re trying to override doesn\'t exists.');
                }

                break;
            case 'property':
                if (!property_exists($object, $target)) {
                    throw new Exception('The property that you\'re trying to override doesn\'t exists.');
                }

                break;
        }
    }
}
